package cn.bootx.platform.common.websocket;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author xxm
 * @date 2022/3/27
 */
@ComponentScan
@AutoConfiguration
public class WebsocketAutoConfiguration {

}
