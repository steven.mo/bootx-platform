package cn.bootx.platform.baseapi.core.sql.dao;

import cn.bootx.platform.baseapi.core.sql.entity.QuerySql;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xxm
 * @date 2023/3/9
 */
@Mapper
public interface QuerySqlMapper extends BaseMapper<QuerySql> {

}
